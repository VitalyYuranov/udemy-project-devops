terraform {
  backend "s3" {
    bucket         = "vyranau-tfstate"
    key            = "udemy-project.tfstate"
    region         = "eu-central-1"
    encrypt        = true
    dynamodb_table = "vyranau-tfstate-lock"
  }
}

provider "aws" {
  region  = "eu-central-1"
  version = "~> 2.54.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}