variable "prefix" {
  default = "udemy"
}

variable "project" {
  default = "udemy-project"
}

variable "contact" {
  default = "vyranau"
}

variable "db_username" {
  description = "Username for the RDS Postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "vyranau-bastion"
}

variable "ecr_image_api" {
  description = "ECR Image for API"
  default     = "043751989667.dkr.ecr.eu-central-1.amazonaws.com/udemy_project:latest"
}

variable "ecr_image_proxy" {
  description = "ECR Image for API"
  default     = "043751989667.dkr.ecr.eu-central-1.amazonaws.com/udemy_project:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}
